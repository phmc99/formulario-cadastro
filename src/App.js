import { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Form from "./components/Form";
import Welcome from "./components/Welcome";
import "./styles/reset.css";

const App = () => {
  const [isRegistred, setIsRegistred] = useState(false);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Form setIsRegistred={setIsRegistred} />
        </Route>
        {isRegistred ? (
          <Route exact path="/welcome/:user">
            <Welcome />
          </Route>
        ) : (
          <Redirect to="/"></Redirect>
        )}
      </Switch>
    </Router>
  );
};

export default App;
