import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import TextField from "@material-ui/core/TextField";

import { Formulario } from "./style";
import { useHistory } from "react-router-dom";

const Form = ({ setIsRegistred }) => {
  const formSchema = yup.object().shape({
    nome: yup
      .string()
      .required("Nome obrigatório")
      .matches(
        /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/,
        "Nome deve conter apenas letras."
      )
      .max(50, "Nome deve conter no máximo 50 caracteres."),
    email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
    senha: yup
      .string()
      .required("Senha obrigatório")
      .matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha com no mínimo 8 caracteres. Necessário ter letras, números e ao menos um símbolo."
      ),
    confirmaSenha: yup
      .string()
      .required("Confirmação de senha obrigatório")
      .oneOf([yup.ref("senha"), null], "As senhas não são iguais"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const history = useHistory();

  const onSubmit = (data) => {
    setIsRegistred(true);
    history.push(`/welcome/${data.nome}`);
  };

  return (
    <Formulario>
      <h3>Cadastre-se</h3>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TextField
          label="Nome"
          {...register("nome")}
          helperText={errors.nome?.message}
          error={errors.nome ? true : false}
        />

        <TextField
          label="E-mail"
          {...register("email")}
          helperText={errors.email?.message}
          error={errors.email ? true : false}
        />

        <TextField
          label="Senha"
          {...register("senha")}
          type="password"
          helperText={errors.senha?.message}
          error={errors.senha ? true : false}
        />

        <TextField
          label="Confirmar Senha"
          {...register("confirmaSenha")}
          type="password"
          helperText={errors.confirmaSenha?.message}
          error={errors.confirmaSenha ? true : false}
        />
        <button type="submit">Cadastrar</button>
      </form>
    </Formulario>
  );
};

export default Form;
