import styled from "styled-components";

export const Formulario = styled.div`
  width: 300px;
  padding: 20px;

  background-color: #f6f6f6f2;
  border-radius: 5px;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  @keyframes fade {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation: linear 0.5s fade;

  border-left: 4px solid royalblue;

  h3 {
    text-align: center;
    margin: 5px 0;
    font-size: 22px;
    color: royalblue;
  }

  form {
    display: flex;
    flex-direction: column;

    .MuiFormControl-root {
      margin-bottom: 10px;
    }

    button {
      background-color: royalblue;

      color: #f3f3f3;

      &:hover {
        transform: scale(105%);
        transition: 0.5s;
        filter: brightness(1.15);
      }

      &:active {
        transform: scale(95%);
      }
    }
  }

  @media (min-width: 768px) {
    width: 400px;
  }
`;
