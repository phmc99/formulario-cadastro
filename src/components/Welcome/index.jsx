import { useHistory, useParams } from "react-router-dom";
import { WelcomeMenssage } from "./style";

const Welcome = () => {
  const { user } = useParams();
  const history = useHistory();

  const handleBackToForm = () => {
    history.push("/");
  };

  return (
    <WelcomeMenssage>
      <h1>Bem vindx, {user}!</h1>
      <button onClick={handleBackToForm}>Voltar</button>
    </WelcomeMenssage>
  );
};

export default Welcome;
