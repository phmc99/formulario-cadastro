import styled from "styled-components";

export const WelcomeMenssage = styled.div`
  width: 300px;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  text-align: center;

  display: flex;
  flex-direction: column;
  align-items: center;

  @keyframes fade {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation: linear 0.5s fade;

  h1 {
    font-size: 32px;
    color: #f3f3f3;
    text-shadow: 1px 1px 4px midnightblue;
  }

  button {
    background-color: #f3f3f3;

    color: royalblue;

    &:hover {
      transform: scale(105%);
      transition: 0.5s;
      filter: brightness(1.15);
      background-color: royalblue;
      color: #f3f3f3;
      box-shadow: 0 0 5px rgb(0, 0, 0, 0.55);
    }

    &:active {
      transform: scale(95%);
    }
  }
`;
